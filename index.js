document.addEventListener('DOMContentLoaded', function(){
    console.log('Hello world!');  
  
  const rootElement = document.querySelector('#root');
  const section = document.querySelectorAll('section');
  let currentSectionIndex = 0;
  let isTrottled = false;


  
  document.addEventListener('mousewheel', function(event){
      if (isTrottled) return;
      isTrottled = true;

      setTimeout(function(){
          isTrottled = false;
      }, 1000)


      const direction = event.wheelDelta < 0 ? 1 : -1;

      scroll(direction);
        

    //     console.log(currentSectionIndex);



    //    console.log(event.wheelDelta);
        scrollCurrentSection();

       
  })

  function scroll(direction){
    if(direction ===1){
        const isLastSection = currentSectionIndex === section.length -1;
        if (isLastSection) return;
    }else if (direction === -1){
        const firstSection = currentSectionIndex === 0;
        if (firstSection) return;
    }

    currentSectionIndex = currentSectionIndex + direction;
  }

  function scrollCurrentSection(){
    section[currentSectionIndex].scrollIntoView({
        behavior: "smooth",
         block: "start"
    });
  }

  })